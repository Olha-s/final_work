let tabs = document.querySelectorAll('.tabs-title');
let contents = document.querySelectorAll('.tab-content');

tabs.forEach(function(tab, i) {
    tab.addEventListener('click', function () {
        document.querySelector('.tab-content.open').classList.remove('open');
        document.querySelector('.tabs-title.active').classList.remove('active');
        tabs[i].classList.toggle('active');
        contents[i].classList.toggle('open');
    });
}) ;
let buttons = document.querySelectorAll('.tabs-menu');
let button3 = document.querySelector('.btn3');

buttons.forEach(function(btn){
    btn.addEventListener('click', function () {
        let currentTabData = document.querySelector(`.tab-images[data-tab-images = "${btn.dataset.tabMenu }"]`);
        document.querySelector('.tab-images.open').classList.remove('open');
        document.querySelector('.tabs-menu.active').classList.remove('active');
        document.querySelector('.tab-images.add').classList.remove('open');

        button3.classList.remove('show');
        currentTabData.classList.add('open');
        btn.classList.add('active');
       if(currentTabData === document.querySelector('.tab-images[data-tab-images ="All"]')){
           button3.classList.add('show');
        }
    });
});

button3.addEventListener('click', function () {
    button3.classList.remove('show');
    document.querySelector('.preload').classList.remove('invisible');


    setTimeout(function (){
        document.querySelector('.preload').classList.add('invisible');
        document.querySelector('.tab-images.add').classList.add('open');
    }, 6000);

});

window.addEventListener('scroll', function () {
if(window.pageYOffset >= screen.height){
    document.querySelector('.back-to-top').style.opacity = "1";
}else{
    document.querySelector('.back-to-top').style.opacity = "0";
}
});

document.querySelector('.back-to-top').addEventListener('click', function() {
      window.scrollTo(0, 0);
   });
